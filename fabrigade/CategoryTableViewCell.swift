//
//  CategoryTableViewCell.swift
//  fabrigade
//
//  Created by Abubakar on 11/24/15.
//  Copyright © 2015 Fabbrigade. All rights reserved.
//

import UIKit

protocol CategoryTableViewCellDelegate: class {
    func categoryTableViewCell(cell: CategoryTableViewCell, didSelectCategory category: Category)
}

class CategoryTableViewCell: UITableViewCell {

    static let identifier = "categoryCell"
    
    var delegate: CategoryTableViewCellDelegate!
    var category: Category? {
        didSet {
            if category == nil {
                return
            }
            
            if category!.enabled {
                self.bookButton.hidden = false
                self.bookButton.enabled = true
                self.bookButton.setTitle("BOOK NOW", forState: .Normal)
            } else {
                self.bookButton.hidden = true
                self.bookButton.enabled = false
                self.bookButton.setTitle("COMING SOON", forState: .Normal)
            }
            
            if let categoryBackgroundImage = self.category!["image"] as? PFFile {
                categoryBackgroundImage.getDataInBackgroundWithBlock({ (data, error) -> Void in
                    if let data = data {
                        let image = UIImageView(image: UIImage(data: data))
                        image.frame = CGRectMake(image.frame.origin.x, image.frame.origin.y, self.contentView.frame.width, self.contentView.frame.height)
                        self.contentView.insertSubview(image, belowSubview: self.bookButton)
                    }
                })
            }
        }
    }
    
    @IBOutlet weak var categoryNameLabel: UILabel!
    @IBOutlet weak var bookButton: UIButton!
    
    @IBAction func bookButtonPressed(sender: UIButton) {
        self.delegate.categoryTableViewCell(self, didSelectCategory: self.category!)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}
