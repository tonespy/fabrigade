//
//  CategoriesTableViewController.swift
//  fabrigade
//
//  Created by Abubakar on 11/24/15.
//  Copyright © 2015 Fabbrigade. All rights reserved.
//

import UIKit

class CategoriesTableViewController: FabBrigadeViewController, BookingRater, BookingDateTimeDisplayDelegate {

    let dateFormatter: NSDateFormatter
    let timeFormatter: NSDateFormatter
    
    required init?(coder aDecoder: NSCoder) {
        self.dateFormatter = NSDateFormatter()
        self.timeFormatter = NSDateFormatter()
        
        super.init(coder: aDecoder)
        self.dateFormatter.dateFormat = dateFormat
        self.timeFormatter.dateFormat = timeFormat
    }
    
    private let showLoginSegueIdentifier = "showLogin"
    private let showProfileSegueIdentifier = "showProfile"
    private let showServicesSegueIdentifer = "showServices"
    
    private var categories: [Category]! {
        didSet {
            self.categoryTableView.reloadData()
        }
    }
    
    private var selectedServices: [Service] = []
    
    @IBOutlet weak var profileButton: UIBarButtonItem!
    @IBOutlet weak var categoryTableView: UITableView!
    
    private var bookingsToRate: [Booking] = [] {
        didSet {
            if self.bookingsToRate.isEmpty {
                self.bookingRaterViewContainer.hidden = true
            } else {
                self.bookingRaterScrollView.contentOffset = CGPointZero
                self.bookingRaterViewContainer.hidden = false
                self.stars = 0
                self.ratingCommentsTextView.text = "Leave an optional review..."
                self.brigadetteToRateProfilePictureView.profileID = self.bookingToRate.brigadette!["facebookId"] as! String
                let name = (self.bookingToRate.brigadette!["name"] as! String).characters.split(isSeparator: {$0 == " "}).map(String.init).first!
                self.bookingToRateDescriptionLabel.text = "Rate " + name + " for your booking on " + self.formatDate(self.bookingToRate.date!)
            }
        }
    }
    
    var bookingToRate: Booking! {
        return self.bookingsToRate.first
    }
    
    var bookingRaterDelegate: BookingRaterDelegate!
    var stars: Int = 0 {
        didSet {
            for i in 0..<stars {
                self.starButtons[i].setImage(UIImage(named: "Star Filled"), forState: .Normal)
            }
            for i in stars..<self.starButtons.count {
                self.starButtons[i].setImage(UIImage(named: "Star"), forState: .Normal)
            }
        }
    }
    
    var comments: String {
        return self.ratingCommentsTextView.text
    }
    
    @IBOutlet weak var bookingRaterViewContainer: UIView!
    @IBOutlet weak var bookingRaterScrollView: UIScrollView!
    @IBOutlet weak var bookingRaterView: UIView!
    @IBOutlet weak var brigadetteToRateProfilePictureView: FBSDKProfilePictureView!
    @IBOutlet weak var bookingToRateDescriptionLabel: UILabel!
    @IBOutlet var starButtons: [UIButton]!
    @IBOutlet weak var ratingCommentsTextView: UITextView!
    @IBOutlet weak var rateButton: FabBrigadeButton!
    
    @IBAction func starButtonPressed(sender: UIButton) {
        if let index = self.starButtons.indexOf(sender) {
            self.stars = index + 1
        }
    }
    
    @IBAction func submitRating() {
        self.ratingCommentsTextView.resignFirstResponder()
        self.rateButton.setTitle("SAVING", forState: .Normal)
        self.rate()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.bookingRaterViewContainer.backgroundColor = UIColor.darkGrayColor().colorWithAlphaComponent(0.5)
        self.bookingRaterView.backgroundColor = Appearance.lightGray
        self.brigadetteToRateProfilePictureView.layer.cornerRadius = self.brigadetteToRateProfilePictureView.layer.frame.width / 2
        self.brigadetteToRateProfilePictureView.clipsToBounds = true
        
        //get bookings to rate
        self.bookingRaterDelegate = self
        if let user = PFUser.currentUser() {
            if !PFAnonymousUtils.isLinkedWithUser(user) {
                let query = PFQuery(className: Booking.parseClassName())
                query.whereKey("client", equalTo: user)
                query.whereKey("status", equalTo: Status.Ended.rawValue)
                query.whereKeyDoesNotExist("rating")
                query.includeKey("brigadette")
                
                query.findObjectsInBackgroundWithBlock({ (objects, error) -> Void in
                    if error == nil {
                        if let bookings = objects as? [Booking] {
                            self.bookingsToRate = bookings
                            self.bookingRaterView.hidden = self.bookingsToRate.isEmpty
                        }
                    }
                })
            }
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationItem.title = "FABBRIGADE"
        
        if self.categories == nil {
            //@todo: show loading
            self.fetchCategories()
        }
        
        if PFAnonymousUtils.isLinkedWithUser(PFUser.currentUser()) {
            self.profileButton.title = "LOGIN"
        } else {
            self.profileButton.title = "ME"
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        self.bookingRaterScrollView.contentSize = CGSize(width: self.bookingRaterView.frame.width, height: self.bookingRaterView.frame.height)
    }
    
    @IBAction func profileButtonPressed(sender: UIBarButtonItem) {
        if PFAnonymousUtils.isLinkedWithUser(PFUser.currentUser()) {
            self.performSegueWithIdentifier(self.showLoginSegueIdentifier, sender: self.profileButton)
        } else {
            self.performSegueWithIdentifier(self.showProfileSegueIdentifier, sender: self.profileButton)
        }
    }
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        self.navigationItem.title = ""
        
        if segue.identifier == self.showServicesSegueIdentifer {
            let servicesController = segue.destinationViewController as! ServicesViewController
            self.navigationItem.title = ""
            servicesController.category = sender as! Category
            servicesController.selectedServices = self.selectedServices
            servicesController.delegate = self
        } else if segue.identifier == self.showLoginSegueIdentifier {
            let loginViewController = segue.destinationViewController as! LoginViewController
            loginViewController.delegate = self
        }
    }
    
    private func fetchCategories() {
        let query = PFQuery(className: Category.parseClassName())
        
        query.whereKeyDoesNotExist("parent")
        query.orderByAscending("position")
        query.findObjectsInBackgroundWithBlock { (results, error) -> Void in
            if error == nil {
                self.categories = results as! [Category]
                //stop loading and display categories
            } else {
                //@todo: handle error
                print(error)
            }
        }
    }
}

extension CategoriesTableViewController: UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.categories == nil ? 0 : self.categories.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(CategoryTableViewCell.identifier, forIndexPath: indexPath) as! CategoryTableViewCell
        
        cell.category = self.categories[indexPath.row]
        cell.delegate = self
        
        return cell
    }
}

extension CategoriesTableViewController: CategoryTableViewCellDelegate {
    func categoryTableViewCell(cell: CategoryTableViewCell, didSelectCategory category: Category) {
        if category.enabled {
            self.performSegueWithIdentifier(self.showServicesSegueIdentifer, sender: category)
        }
    }
}

extension CategoriesTableViewController: LoginViewControllerDelegate {
    func loginViewController(controller: LoginViewController, didLoginUser: PFUser) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func didDismissLoginViewController(controller: LoginViewController) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}

extension CategoriesTableViewController: ServicesViewControllerDelegate {
    func servicesViewController(servicesViewController: ServicesViewController, didSelectService selectedService: Service) {
        self.selectedServices.append(selectedService)
    }
    
    func servicesViewController(servicesViewController: ServicesViewController, didDeSelectService deselectedService: Service) {
        if let serviceIndex = self.selectedServices.indexOf(deselectedService) {
            self.selectedServices.removeAtIndex(serviceIndex)
        }
    }
}

extension CategoriesTableViewController: BookingRaterDelegate {
    func bookingRater(bookingRater: BookingRater, didRateBooking booking: Booking) {
        //check if more bookings to rate, else hide
        self.rateButton.setTitle("FINISH", forState: .Normal)
        self.bookingsToRate.removeFirst()
    }
    
    func bookingRater(bookingRater: BookingRater, failedToRateBooking booking: Booking) {
        //show error
        print("failed to rate booking")
        self.rateButton.setTitle("FINISH", forState: .Normal)
    }
}

extension CategoriesTableViewController: UITextViewDelegate {
    func textViewShouldBeginEditing(textView: UITextView) -> Bool {
        textView.text = ""
        self.bookingRaterScrollView.contentSize = CGSize(width: self.bookingRaterView.frame.width, height: self.bookingRaterView.frame.height + self.ratingCommentsTextView.frame.height + self.rateButton.frame.height + 25)
        self.bookingRaterScrollView.contentOffset = CGPoint(x: 0, y: self.ratingCommentsTextView.frame.minY)
        return true
    }
}
