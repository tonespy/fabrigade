//
//  Booking.swift
//  FabBrigade
//
//  Created by Kareem Sabri on 2015-09-01.
//  Copyright (c) 2015 FabBrigade. All rights reserved.
//

import Foundation
import Parse

protocol DurationRepresenter: class {
    func durationInHoursAndMinutes(duration durationInMinutes: Int) -> (hours: Int, minutes: Int)
    func durationStringInHoursAndMinutes(duration durationInMinutes: Int) -> String
}

protocol PriceRepresenter: class {
    var price: Int { get }
    func priceStringInBaht() -> String
}

extension PriceRepresenter {
    func priceStringInBaht() -> String {
        let numberFormatter = NSNumberFormatter()
        numberFormatter.formatterBehavior = NSNumberFormatterBehavior.Behavior10_4
        numberFormatter.numberStyle = NSNumberFormatterStyle.DecimalStyle
        return numberFormatter.stringFromNumber(self.price)! + "฿"
    }
}

extension DurationRepresenter {
    final func durationInHoursAndMinutes(duration durationInMinutes: Int) -> (hours: Int, minutes: Int) {
        return (hours: durationInMinutes / 60, minutes: durationInMinutes % 60)
    }
    
    func durationStringInHoursAndMinutes(duration durationInMinutes: Int) -> String {
        let hoursAndMinutes = self.durationInHoursAndMinutes(duration: durationInMinutes)
        
        var durationString  = ""
        
        if hoursAndMinutes.hours > 0 {
            durationString.appendContentsOf(String(hoursAndMinutes.hours) + "HR")
        }
        
        if hoursAndMinutes.minutes > 0 {
            durationString.appendContentsOf(String(hoursAndMinutes.minutes) + "MIN")
        }
        
        return durationString
    }
}

enum Status: Int, CustomStringConvertible {
    case Open = 1
    case Confirmed = 2
    case Started = 3
    case Ended = 4
    case CancelledByCustomer = 10
    case CancelledByBrigadette = 11
    case NoMatch = 20
    
    var description: String {
        switch self {
        case .Open:
            return "Finding Brigadette"
        case .Confirmed:
            return "Confirmed"
        case .Started:
            return "Started"
        case .Ended:
            return "Ended"
        case .CancelledByCustomer:
           return "Cancelled"
        case .CancelledByBrigadette:
            return "Cancelled"
        case .NoMatch:
            return "Timed Out"
        }
    }
}

class Booking: PFObject, PFSubclassing, DurationRepresenter, PriceRepresenter {
    @NSManaged var date: NSDate?
    @NSManaged var address: String?
    @NSManaged var apartment: String?
    @NSManaged var city: String?
    @NSManaged var zip: String?
    @NSManaged var phoneNumber: String?
    
    @NSManaged var creditCard: CreditCard
    @NSManaged var client: PFUser
    @NSManaged var brigadette: PFUser?
    @NSManaged var services: [Service]
    @NSManaged var status: Int
    
    @NSManaged var rating: Rating
    
    var endDate: NSDate? {
        get {
            if !self.hasDate() {
                return nil
            }
            
            let gregorian = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)
            let components = NSDateComponents()
            
            components.minute = self.durationInMinutes()
            
            return gregorian!.dateByAddingComponents(components, toDate: self.date!, options: NSCalendarOptions())
        }
    }
    
    var descriptionOfServices: String {
        get {
            return self.services.map({ (service: Service) -> String in
                return service.name
            }).joinWithSeparator(", ")
        }
    }
    
    override class func initialize() {
        CreditCard.initialize()
        Service.initialize()
        struct Static {
            static var onceToken: dispatch_once_t = 0
        }
        
        dispatch_once(&Static.onceToken) {
            self.registerSubclass()
        }
    }
    
    static func parseClassName() -> String {
        return "Booking"
    }
    
    func durationInMinutes() -> Int {
        return self.services.reduce(0) { (sum: Int, service: Service) -> Int in
            return sum + service.duration
        }
    }
    
    var price: Int {
        return self.services.reduce(0) { (sum: Int, service: Service) -> Int in
            return sum + service.price
        }
    }
    
    func hasValidLocation() -> Bool {
        //@todo: validate location
        return self.address != nil && self.city != nil && self.zip != nil
    }
    
    func hasDate() -> Bool {
        return self.date != nil
    }
    
    func hasDateInFuture() -> Bool {
        return self.hasDate() && self.date!.compare(NSDate()) == NSComparisonResult.OrderedDescending
    }
    
    func cancel() {
        self.status = Status.CancelledByCustomer.rawValue
    }
}
