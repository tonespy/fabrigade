//
//  ServiceTableViewCell.swift
//  FabBrigade
//
//  Created by Kareem Sabri on 2015-09-02.
//  Copyright (c) 2015 FabBrigade. All rights reserved.
//

import UIKit

class ServiceViewCell: UITableViewCell {
    var service: Service!
    
    var index: NSIndexPath!
    weak var tableView: UITableView!
    
    @IBOutlet weak var serviceNameLabel: UILabel!
    @IBOutlet weak var serviceDescriptionLabel: UILabel!
    @IBOutlet weak var serviceDurationLabel: UILabel!
    @IBOutlet weak var servicePriceLabel: UILabel!
    @IBOutlet weak var selectOrDeselectButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectOrDeselectButton.setTitleColor(Appearance.darkPink, forState: .Normal)
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
        if self.selected {
            self.selectOrDeselectButton.setTitle("REMOVE", forState: .Normal)
        } else {
            self.selectOrDeselectButton.setTitle("GET THIS LOOK", forState: .Normal)
        }
    }
    
    @IBAction func selectOrDeselectButtonPressed(sender: UIButton) {
        self.setSelected(!self.selected, animated: false)
        if self.selected {
            self.tableView.delegate!.tableView!(self.tableView, didSelectRowAtIndexPath: self.index)
        } else {
            self.tableView.delegate!.tableView!(self.tableView, didDeselectRowAtIndexPath: self.index)
        }
    }
    
}
