//
//  ViewController.swift
//  fabrigade
//
//  Created by Abubakar on 11/23/15.
//  Copyright © 2015 Fabbrigade. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        for family: String in UIFont.familyNames() {
            print(family)
            for names in UIFont.fontNamesForFamilyName(family) {
                print("==\(names)")
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

