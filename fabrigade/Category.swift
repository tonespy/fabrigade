//
//  Category.swift
//  FabBrigade
//
//  Created by Kareem Sabri on 2015-08-31.
//  Copyright (c) 2015 FabBrigade. All rights reserved.
//

import Foundation
import Parse

class Category: PFObject, PFSubclassing {
    @NSManaged var enabled: Bool
    @NSManaged var name: String
    @NSManaged var position: Int
    @NSManaged var parent: Category?
    
    override class func initialize() {
        struct Static {
            static var onceToken: dispatch_once_t = 0
        }
        
        dispatch_once(&Static.onceToken) {
            self.registerSubclass()
        }
    }
    
    static func parseClassName() -> String {
        return "Category"
    }
}