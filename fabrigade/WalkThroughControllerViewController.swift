//
//  WalkThroughControllerViewController.swift
//  fabrigade
//
//  Created by Abubakar on 11/23/15.
//  Copyright © 2015 Fabbrigade. All rights reserved.
//

import UIKit

class WalkThroughControllerViewController: UIViewController {

    @IBOutlet weak var walkThroughScroll: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let walkThrough =  LAWalkthroughViewController()
        walkThrough.view.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)
        walkThrough.addPageWithNibName("Fabbrigade", bundle: nil)
        walkThrough.addPageWithNibName("LookAndBook", bundle: nil)
        walkThrough.addPageWithNibName("EasyPeasy", bundle: nil)
        walkThrough.nextButtonText = ""
        self.addChildViewController(walkThrough)
        self.view.addSubview(walkThrough.view)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func generateView(header: String, below: String) -> UIView {
        let size: CGSize = self.view.frame.size
        let mainView = UIView(frame: CGRectMake(0, 0, size.width, size.height))
        mainView.backgroundColor = UIColor.clearColor()
        let contentHolderView = UIView(frame: CGRectMake(0, 0, 350, 172))
        contentHolderView.backgroundColor = UIColor.clearColor()
        let headerLabel = UILabel(frame: CGRectMake(38, 24, 298, 34))
        let belowLabel = UILabel(frame: CGRectMake(14, 66, 344, 83))
        belowLabel.numberOfLines = 0
        
        headerLabel.text = header
        belowLabel.text = below
        
        headerLabel.textColor = UIColor.blackColor()
        belowLabel.textColor = UIColor.blackColor()
        
        headerLabel.font = UIFont(name: "BrandonGrotesque-Bold", size: 30)
        belowLabel.font = UIFont(name: "BrandonGrotesque-Light", size: 20)
        
        headerLabel.textAlignment = .Center
        belowLabel.textAlignment = .Center
        
        contentHolderView.addSubview(headerLabel)
        contentHolderView.addSubview(belowLabel)
        contentHolderView.center = mainView.center
        mainView.addSubview(contentHolderView)
        //mainView.center = self.view.center
        //self.view.addSubview(mainView)
        return mainView
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
