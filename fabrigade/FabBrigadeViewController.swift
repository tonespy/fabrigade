//
//  FabBrigadeViewController.swift
//  fabrigade
//
//  Created by Abubakar on 11/24/15.
//  Copyright © 2015 Fabbrigade. All rights reserved.
//

import UIKit

struct Appearance {
    static let lightPink = UIColor(red: 245/255, green:233/255, blue: 230/255, alpha: 1.0)
    static let mediumPink = UIColor(red: 234/255, green: 196/255, blue: 188/255, alpha: 1.0)
    static let darkPink = UIColor(red: 230/255, green: 121/255, blue: 106/255, alpha: 1.0)
    static let lightGray = UIColor(red: 247/255, green: 247/255, blue: 247/255, alpha: 1.0)
    
    static let red = UIColor(red: 219/255, green: 106/255, blue: 97/255, alpha: 1.0)
    static let green = UIColor(red: 135/255, green: 183/255, blue: 67/255, alpha: 1.0)
    static let yellow = UIColor(red: 242/255, green: 207/255, blue: 64/255, alpha: 1.0)
}

class FabBrigadeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.barTintColor = Appearance.lightPink
        self.view.backgroundColor = Appearance.lightGray
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
