//
//  ServicesViewController.swift
//  fabrigade
//
//  Created by Abubakar on 11/24/15.
//  Copyright © 2015 Fabbrigade. All rights reserved.
//

import Parse
import UIKit

protocol ServicesViewControllerDelegate {
    func servicesViewController(servicesViewController: ServicesViewController, didSelectService selectedService: Service)
    func servicesViewController(servicesViewController: ServicesViewController, didDeSelectService deSelectedService: Service)
}

class ServicesViewController: UIViewController {
    
    @IBOutlet weak var childCategorySegementedControl: UISegmentedControl!
    
    private let showLoginSegueIdentifier = "showLogin"
    private let newBookingSegueIdentifier = "newBookingSegue"
    private let showBookingSegueIdentifier = "showBooking"
    private var bookingInProgess: Booking?
    private var badge: GIBadgeView!
    
    var delegate: ServicesViewControllerDelegate!
    
    var category: Category! {
        didSet {
            self.services = nil
            self.fetchCategories()
        }
    }
    
    private var childCategories: [Category] = [] {
        didSet {
            self.childCategorySegementedControl.removeAllSegments()
            for i in 0..<self.childCategories.count {
                let categoryName = self.childCategories[i].name.uppercaseString
                self.childCategorySegementedControl.insertSegmentWithTitle(categoryName, atIndex: i, animated: false)
            }
            self.childCategorySegementedControl.selectedSegmentIndex = 0
            self.fetchServices()
        }
    }

    @IBAction func didPressChildCategoryButton(sender: AnyObject) {
        let categoryIndex = self.childCategorySegementedControl.selectedSegmentIndex
        self.childCategory = self.childCategories[categoryIndex]
        self.servicesTableView?.reloadData()
        self.reselectSelectedServices()
    }
    
    private var childCategory: Category! {
        didSet {
            self.servicesTableView?.reloadData()
            self.reselectSelectedServices()
        }
    }
    
    private var services: [Service]! {
        didSet {
            if self.childCategory == nil {
                self.childCategory = self.childCategories.first
            }
        }
    }
    
    var selectedServices: [Service] = []
    
    private let servicesTableViewCellReuseIdentifier = "serviceTableViewCell"
    @IBOutlet weak var servicesTableView: UITableView!
    
    @IBOutlet weak var numberOfServicesSelectedLabel: UILabel!
    @IBOutlet weak var requestBookingButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.servicesTableView.registerClass(ServiceViewCell.self, forCellReuseIdentifier: self.servicesTableViewCellReuseIdentifier)
        self.servicesTableView.registerNib(UINib(nibName: "ServiceViewCell", bundle: nil), forCellReuseIdentifier: self.servicesTableViewCellReuseIdentifier)
        self.servicesTableView.allowsMultipleSelection = true
        
        self.badge = GIBadgeView()
        self.badge.backgroundColor = Appearance.darkPink
        
        self.childCategorySegementedControl.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.darkGrayColor()], forState: .Normal)
        self.childCategorySegementedControl.setTitleTextAttributes([NSForegroundColorAttributeName: Appearance.darkPink], forState: .Selected)
        self.childCategorySegementedControl.tintColor = UIColor.whiteColor()
        self.childCategorySegementedControl.backgroundColor = UIColor.whiteColor()
        self.childCategorySegementedControl.addTarget(self, action: "didPressChildCategoryButton:", forControlEvents: .ValueChanged)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationItem.title = "SELECT SERVICES"
        
        let shoppingCartView = UIImageView(image: UIImage(named: "Shopping Bag"))
        shoppingCartView.userInteractionEnabled = true
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: "requestBooking")
        shoppingCartView.addGestureRecognizer(tapGestureRecognizer)
        shoppingCartView.addSubview(self.badge)
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: shoppingCartView)
        self.navigationItem.rightBarButtonItem!.target = self
        self.navigationItem.rightBarButtonItem!.action = "requestBooking"
        
        if self.bookingInProgess == nil {
            self.bookingInProgess = Booking()
        }
        
        self.badge.badgeValue = self.selectedServices.count
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationItem.title = ""
    }
    
    private func fetchServices() {
        let query = PFQuery(className: Service.parseClassName())
        
        query.whereKey("category", containedIn: self.childCategories)
        query.findObjectsInBackgroundWithBlock { (results, error) -> Void in
            if error == nil {
                self.services = results as! [Service]
            } else {
                //@todo: handle error
            }
        }
    }
    
    private func fetchCategories() {
        let query = PFQuery(className: Category.parseClassName())
        
        query.whereKey("parent", equalTo: self.category)
        query.findObjectsInBackgroundWithBlock { (results, error) -> Void in
            if error == nil {
                self.childCategories = results as! [Category]
            } else {
                //@todo: handle error
            }
        }
    }
    
    @IBAction func requestBooking() {
        if self.selectedServices.count == 0 {
            //@todo: tell them to select services
            return
        }
        
        if PFAnonymousUtils.isLinkedWithUser(PFUser.currentUser()) {
            self.performSegueWithIdentifier(self.showLoginSegueIdentifier, sender: self.requestBookingButton)
            return
        }
        
        self.bookingInProgess!.services = self.selectedServices
        
        self.performSegueWithIdentifier(self.newBookingSegueIdentifier, sender: self.requestBookingButton)
    }
    
//    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
//        if segue.identifier == self.showBookingSegueIdentifier {
//            self.navigationItem.title = ""
//            let viewBookingViewController = segue.destinationViewController as! ViewBookingViewController
//            viewBookingViewController.booking = self.bookingInProgess!
//            self.bookingInProgess = nil
//        } else if segue.identifier == self.newBookingSegueIdentifier {
//            self.navigationItem.title = ""
//            let newBookingViewController = segue.destinationViewController as! NewBookingViewController
//            newBookingViewController.booking = self.bookingInProgess!
//            newBookingViewController.delegate = self
//        } else if segue.identifier == self.showLoginSegueIdentifier {
//            self.navigationItem.title = ""
//            let loginViewController = segue.destinationViewController as! LoginViewController
//            loginViewController.delegate = self
//        }
//    }
}

extension ServicesViewController: UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.services == nil ? 0 : self.services.filter({ (service: Service) -> Bool in
            return service.category.objectId! == self.childCategory.objectId!
        }).count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return self.view.window == nil ? 1 : self.view.window!.frame.height * 0.75
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let service = self.serviceAtIndexPath(indexPath)
        
        let cell = tableView.dequeueReusableCellWithIdentifier(self.servicesTableViewCellReuseIdentifier) as! ServiceViewCell
        cell.selectionStyle = .None
        cell.tableView = tableView
        cell.index = indexPath
        cell.service = service
        cell.serviceNameLabel.text = service.name.uppercaseString
        cell.serviceDescriptionLabel.text = service.serviceDescription
        cell.serviceDurationLabel.text = service.durationStringInHoursAndMinutes(duration: service.duration)
        cell.servicePriceLabel.text = service.priceStringInBaht()
        
        if let serviceBackgroundImage = service["image"] as? PFFile {
            serviceBackgroundImage.getDataInBackgroundWithBlock({ (data, error) -> Void in
                if let data = data {
                    cell.backgroundView = UIImageView(image: UIImage(data: data))
                    cell.selectedBackgroundView = UIImageView(image: UIImage(data: data))
                }
            })
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.selectedServices.append(self.serviceAtIndexPath(indexPath))
        self.badge.badgeValue = self.selectedServices.count
        self.delegate.servicesViewController(self, didSelectService: self.selectedServices.last!)
    }
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        if let serviceIndex = self.selectedServices.indexOf(self.serviceAtIndexPath(indexPath)) {
            let removedService = self.selectedServices.removeAtIndex(serviceIndex)
            self.badge.badgeValue = self.selectedServices.count
            
            self.delegate.servicesViewController(self, didDeSelectService: removedService)
        }
    }
    
    private func serviceAtIndexPath(indexPath: NSIndexPath) -> Service {
        return self.services.filter({(service: Service) -> Bool in
            service.category.objectId! == self.childCategory.objectId!
        })[indexPath.row]
    }
    
    private func indexOfService(service: Service) -> Int? {
        return self.services.filter({(service: Service) -> Bool in
            service.category.objectId! == self.childCategory.objectId!
        }).indexOf({ (otherService) -> Bool in
            return service.objectId == otherService.objectId
        })
    }
    
    private func priceOfSelectedServices() -> Int {
        return self.selectedServices.reduce(0, combine: { (sum: Int, service: Service) -> Int in
            return sum + service.price
        })
    }
    
    private func durationOfSelectedServices() -> Int {
        return self.selectedServices.reduce(0, combine: { (sum: Int, service: Service) -> Int in
            return sum + service.duration
        })
    }
    
    private func reselectSelectedServices() {
        for service in self.selectedServices {
            if let index = self.indexOfService(service) {
                self.servicesTableView.selectRowAtIndexPath(NSIndexPath(forRow: index, inSection: 0), animated: false, scrollPosition: UITableViewScrollPosition.None)
            }
        }
    }
}

extension ServicesViewController: UITableViewDelegate {
}

extension ServicesViewController: LoginViewControllerDelegate {
    func loginViewController(controller: LoginViewController, didLoginUser: PFUser) {
        self.dismissViewControllerAnimated(true, completion: {
            self.requestBooking()
        })
    }
    
    func didDismissLoginViewController(controller: LoginViewController) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}

//extension ServicesViewController: NewBookingViewControllerDelegate {
//    func newBookingViewController(newBookingViewController: NewBookingViewController, didCreateBooking booking: Booking) {
//        for service in self.selectedServices {
//            self.delegate.servicesViewController(self, didDeSelectService: service)
//        }
//        
//        self.selectedServices.removeAll()
//        self.servicesTableView.reloadData()
//        self.bookingInProgess = booking
//        self.navigationController?.viewControllers.popLast()
//        self.performSegueWithIdentifier(self.showBookingSegueIdentifier, sender: self.requestBookingButton)
//    }
//}
