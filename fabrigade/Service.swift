//
//  Service.swift
//  FabBrigade
//
//  Created by Kareem Sabri on 2015-08-31.
//  Copyright (c) 2015 FabBrigade. All rights reserved.
//

import Foundation
import Parse

class Service: PFObject, PFSubclassing, DurationRepresenter, PriceRepresenter {
    @NSManaged var category: Category
    @NSManaged var name: String
    @NSManaged var serviceDescription: String
    @NSManaged var duration: Int
    @NSManaged var price: Int
    @NSManaged var position: Int
    
    override class func initialize() {
        struct Static {
            static var onceToken: dispatch_once_t = 0
        }
        
        dispatch_once(&Static.onceToken) {
            self.registerSubclass()
        }
    }
    
    static func parseClassName() -> String {
        return "Service"
    }
}