//
//  LoginViewController.swift
//  fabrigade
//
//  Created by Abubakar on 11/24/15.
//  Copyright © 2015 Fabbrigade. All rights reserved.
//

import UIKit

protocol LoginViewControllerDelegate: class {
    func loginViewController(controller: LoginViewController, didLoginUser: PFUser)
    func didDismissLoginViewController(controller: LoginViewController)
}

enum State: Int {
    case Login = 0, SignUp, Reset
}

class LoginViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var connectFacebookButton: UIButton!
    @IBOutlet weak var emailAddressTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var noAccountButton: UIButton!
    
    var isKeyboardVisible: Bool = false
    
    @IBAction func close() {
        self.delegate.didDismissLoginViewController(self)
    }

    var state: State = .Login
    var delegate: LoginViewControllerDelegate!
    
    override func viewDidLoad() {//F0B7AE
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationItem.title = "LOGIN"
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        //self.scrollView.contentSize = CGSize(width: self.contentView.frame.width, height: self.contentView.frame.height + 500)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector(keyboardAppeared()), name: UIKeyboardDidShowNotification, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func keyboardAppeared() {
        if isKeyboardVisible == false {
            isKeyboardVisible == true
        }
    }
    
    @IBAction func connectFacebook(sender: UIButton) {
        PFFacebookUtils.logInInBackgroundWithReadPermissions(["email"]) {
            (user: PFUser?, error: NSError?) -> Void in
            if let user = user {
                user.saveInBackgroundWithBlock({ (success, error) -> Void in
                    if error == nil {
                        FBSDKGraphRequest.init(graphPath: "me", parameters: ["fields": "name,email"]).startWithCompletionHandler({ (connection, result, error) -> Void in
                            if error == nil {
                                user.setValue(result["id"]!, forKey: "facebookId")
                                user.setValue(result["name"]!, forKey: "name")
                                user.setValue(result["email"], forKey: "email")
                                user.saveInBackground()
                                self.delegate.loginViewController(self, didLoginUser: user)
                            } else {
                                print(error)
                            }
                        })
                    } else {
                        //@todo: handle error
                        print(error)
                    }
                })
            } else {
                //print("Uh oh. The user cancelled the Facebook login.")
            }
        }
    }
    
    @IBAction func loginButtonTapped() {
        let email = self.emailAddressTextField.text
        let password = self.passwordTextField.text
        let confirmedPassword = self.confirmPasswordTextField.text
        
        if self.state == .Login {
            //logging in
            if ![email, password].filter({$0 == nil || $0!.isEmpty}).isEmpty {
                let alertController = UIAlertController(title: "Whoops!", message: "Please fill out both form fields.", preferredStyle: .Alert)
                alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
                self.presentViewController(alertController, animated: true, completion: nil)
            } else {
                PFUser.logInWithUsernameInBackground(email!, password: password!) {
                    (user: PFUser?, error: NSError?) -> Void in
                    if user != nil {
                        self.delegate.loginViewController(self, didLoginUser: user!)
                    } else {
                        let alertController = UIAlertController(title: "Whoops!", message: "Login failed. Please check your email and password.", preferredStyle: .Alert)
                        alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
                        self.presentViewController(alertController, animated: true, completion: nil)
                    }
                }
            }
        } else if self.state == .SignUp {
            //signing up
            if ![email, password, confirmedPassword].filter({$0 == nil || $0!.isEmpty}).isEmpty {
                let alertController = UIAlertController(title: "Whoops!", message: "Please fill out all form fields.", preferredStyle: .Alert)
                alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
                self.presentViewController(alertController, animated: true, completion: nil)
            } else if password! != confirmedPassword! {
                let alertController = UIAlertController(title: "Whoops!", message: "Passwords do not match.", preferredStyle: .Alert)
                alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
                self.presentViewController(alertController, animated: true, completion: nil)
            } else {
                let user = PFUser.currentUser()!
                user.email = email!
                user.username = email!
                user.password = password!
                user["name"] = email!
                
                user.signUpInBackgroundWithBlock {
                    (succeeded: Bool, error: NSError?) -> Void in
                    if let error = error {
                        if let errorString = error.userInfo["error"] as? String {
                            // Show the errorString and let the user try again.
                            let alertController = UIAlertController(title: "Whoops!", message: errorString, preferredStyle: .Alert)
                            alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
                            self.presentViewController(alertController, animated: true, completion: nil)
                        }
                    } else {
                        // Hooray! Let them use the app now.
                        self.delegate.loginViewController(self, didLoginUser: user)
                    }
                }
            }
        } else if self.state == .Reset {
            if email == nil || email!.isEmpty {
                let alertController = UIAlertController(title: "Whoops!", message: "Please enter your email.", preferredStyle: .Alert)
                alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
                self.presentViewController(alertController, animated: true, completion: nil)
            } else {
                PFUser.requestPasswordResetForEmailInBackground(email!, block: { (success, error) -> Void in
                    if let error = error {
                        if let errorString = error.userInfo["error"] as? String {
                            let alertController = UIAlertController(title: "Whoops!", message: errorString, preferredStyle: .Alert)
                            alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
                            self.presentViewController(alertController, animated: true, completion: nil)
                        }
                    } else {
                        let alertController = UIAlertController(title: "Success!", message: "Check your email to reset your password.", preferredStyle: .Alert)
                        alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: {(action) -> Void in
                            self.changeToState(.Login)
                        }))
                        self.presentViewController(alertController, animated: true, completion: nil)
                    }
                })
            }
        }
    }
    
    @IBAction func forgotPasswordButtonTapped() {
        self.changeToState(.Reset)
    }
    
    @IBAction func noAccountButtonTapped() {
        switch self.state {
        case .Login:
            self.changeToState(.SignUp)
        case .SignUp:
            self.changeToState(.Login)
        case .Reset:
            self.changeToState(.Login)
        }
    }
    
    private func changeToState(state: State) {
        switch state {
        case .Login:
            self.emailAddressTextField.hidden = false
            self.passwordTextField.hidden = false
            self.confirmPasswordTextField.hidden = true
            self.forgotPasswordButton.hidden = false
            self.loginButton.setTitle("LOGIN", forState: .Normal)
            self.noAccountButton.setTitle("I don't have an account", forState: .Normal)
        case .SignUp:
            self.emailAddressTextField.hidden = false
            self.passwordTextField.hidden = false
            self.confirmPasswordTextField.hidden = false
            self.forgotPasswordButton.hidden = true
            self.loginButton.setTitle("SIGN UP", forState: .Normal)
            self.noAccountButton.setTitle("I have an account", forState: .Normal)
        case .Reset:
            self.emailAddressTextField.hidden = false
            self.passwordTextField.hidden = true
            self.confirmPasswordTextField.hidden = true
            self.forgotPasswordButton.hidden = true
            self.loginButton.setTitle("RESET", forState: .Normal)
            self.noAccountButton.setTitle("Back To Login", forState: .Normal)
        }
        
        self.state = state
    }

}

extension LoginViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(textField: UITextField) {
        self.scrollView.setContentOffset(CGPoint(x: 0, y: textField.frame.minY - 100), animated: true)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
