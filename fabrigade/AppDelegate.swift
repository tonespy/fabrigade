//
//  AppDelegate.swift
//  fabrigade
//
//  Created by Abubakar on 11/23/15.
//  Copyright © 2015 Fabbrigade. All rights reserved.
//

import Bolts
import Fabric
import Crashlytics
import FBSDKCoreKit
import Parse
import ParseFacebookUtilsV4
import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        #if DEBUG
            let parseConfig: (applicationId: String, clientKey: String) = ("es6YcnjCesANK51tLds7hfdKUxzRMs1xOJRa0oVE", "H7bdg2qbugZj7ztLyAxfe1dFRBiHCaQ4uCel8JDv")
        #else
            let parseConfig: (applicationId: String, clientKey: String) = ("es6YcnjCesANK51tLds7hfdKUxzRMs1xOJRa0oVE", "H7bdg2qbugZj7ztLyAxfe1dFRBiHCaQ4uCel8JDv")
            //let parseConfig: (applicationId: String, clientKey: String) = ("mMVXpN1ih7hZdV8YQrRTe99DCdF6rh7HCjUewAy1", "NBtgaKm0wtEYqHhc1htvrmokFMyufEVb5pTu6T7Q")
        #endif
        
        Parse.setApplicationId(parseConfig.applicationId,
            clientKey: parseConfig.clientKey)
        
        PFFacebookUtils.initializeFacebookWithApplicationLaunchOptions(launchOptions)
        
        PFAnalytics.trackAppOpenedWithLaunchOptions(launchOptions)
        
        PFUser.enableAutomaticUser()
        if PFAnonymousUtils.isLinkedWithUser(PFUser.currentUser()) {
            //user is anonymous - if we have an access token, try to log them in
            if let accessToken = FBSDKAccessToken.currentAccessToken() {
                PFFacebookUtils.logInInBackgroundWithAccessToken(accessToken)
            }
        }
        
        Fabric.with([Crashlytics.self()])
        
        UINavigationBar.appearance().tintColor = UIColor.blackColor()
        print(NSUserDefaults.standardUserDefaults().boolForKey("HasLaunchedOnce"))
        
        if NSUserDefaults.standardUserDefaults().boolForKey("HasLaunchedOnce") {
            if launchOptions != nil {
                if let dictionary = launchOptions![UIApplicationLaunchOptionsRemoteNotificationKey] {
                    if let bookingId = dictionary["bookingId"] as? String {
                        self.showBookingWithBookingId(bookingId)
                    }
                }
            } else {
                self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let initialView = storyBoard.instantiateViewControllerWithIdentifier("mainViewController")
                self.window?.rootViewController = initialView
                self.window?.makeKeyAndVisible()
            }
        } else {
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "HasLaunchedOnce")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        let currentInstallation = PFInstallation.currentInstallation()
        currentInstallation.setDeviceTokenFromData(deviceToken)
        currentInstallation.channels = ["user"]
        currentInstallation.setValue(PFUser.currentUser(), forKey: "user")
        currentInstallation.saveInBackground()
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        if application.applicationState == .Inactive {
            //they opened from the notification - open the booking if exists
            if let bookingId = userInfo["bookingId"] as? String {
                self.showBookingWithBookingId(bookingId)
            }
        } else {
            //the app is running - show alert
            PFPush.handlePush(userInfo)
        }
    }
    
    private func showBookingWithBookingId(bookingId: String) {
        let query = PFQuery(className: Booking.parseClassName())
        query.includeKey("brigadette")
        query.includeKey("creditCard")
        query.includeKey("services")
        query.includeKey("services.category")
        query.includeKey("services.category.parent")
        query.getObjectInBackgroundWithId(bookingId, block: { (booking, error) -> Void in
            if error == nil {
                if let booking = booking as? Booking {
                    /*let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let bookingViewController = storyboard.instantiateViewControllerWithIdentifier("viewBookingViewController") as! ViewBookingViewController
                    bookingViewController.booking = booking
                    bookingViewController.canDismiss = true
                    self.window!.rootViewController!.presentViewController(bookingViewController, animated: true, completion: nil)*/
                }
            }
        })
    }

    // MARK: - Core Data stack

    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.fabbrigade.fabrigade" in the application's documents Application Support directory.
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1]
    }()

    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = NSBundle.mainBundle().URLForResource("fabrigade", withExtension: "momd")!
        return NSManagedObjectModel(contentsOfURL: modelURL)!
    }()

    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey] = failureReason

            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()

    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }

}

