//
//  CreditCard.swift
//  FabBrigade
//
//  Created by Kareem Sabri on 2015-09-19.
//  Copyright © 2015 FabBrigade. All rights reserved.
//

import UIKit

class CreditCard: PFObject, PFSubclassing {
    override class func initialize() {
        struct Static {
            static var onceToken: dispatch_once_t = 0
        }
        
        dispatch_once(&Static.onceToken) {
            self.registerSubclass()
        }
    }
    
    static func parseClassName() -> String {
        return "CreditCard"
    }
    
    @NSManaged var last4Digits: String
    @NSManaged var expiryDate: NSDate
    @NSManaged var user: PFUser
}
