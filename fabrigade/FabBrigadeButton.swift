//
//  FabBrigadeButton.swift
//  fabrigade
//
//  Created by Abubakar on 11/24/15.
//  Copyright © 2015 Fabbrigade. All rights reserved.
//

import UIKit

//@IBDesignable
class FabBrigadeButton: UIButton {

    @IBInspectable var textSize: CGFloat = 20
    @IBInspectable var textFont: String = "BrandonGrotesque-Light"
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.backgroundColor = Appearance.darkPink
        self.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        self.titleLabel?.font = UIFont(name: textFont, size: textSize)
    }

}
