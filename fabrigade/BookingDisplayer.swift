//
//  BookingDisplayer.swift
//  fabrigade
//
//  Created by Abubakar on 11/24/15.
//  Copyright © 2015 Fabbrigade. All rights reserved.
//

import UIKit

protocol BookingDateTimeDisplayDelegate: class {
    var dateFormat: String { get }
    var timeFormat: String { get }
    
    var dateFormatter: NSDateFormatter { get }
    var timeFormatter: NSDateFormatter { get }
    
    func formatDate(date: NSDate) -> String
    func formatTime(date: NSDate) -> String
    func formatDateAndTimeOfBooking(booking: Booking, includeEndTime: Bool) -> String
}

extension BookingDateTimeDisplayDelegate {
    var dateFormat: String {
        return "EEE MMM dd"
    }
    
    var timeFormat: String {
        return "h:mma"
    }
    
    func formatDate(date: NSDate) -> String {
        return self.dateFormatter.stringFromDate(date)
    }
    
    func formatTime(date: NSDate) -> String {
        return self.timeFormatter.stringFromDate(date)
    }
    
    func formatDateAndTimeOfBooking(booking: Booking, includeEndTime: Bool) -> String {
        var formattedDateTimeString = self.formatDate(booking.date!) + " " + self.formatTime(booking.date!)
        if includeEndTime {
            formattedDateTimeString += " - " + self.formatTime(booking.endDate!)
        }
        
        return formattedDateTimeString
    }
}

class BookingDisplayer: UIViewController {

    var booking: Booking!
    var dateFormatter: NSDateFormatter!
    var timeFormatter: NSDateFormatter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.dateFormatter = NSDateFormatter()
        self.dateFormatter.dateFormat = "EEE MMM dd"
        self.timeFormatter = NSDateFormatter()
        self.timeFormatter.dateFormat = "hh:mma"
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.updateBookingInfoDisplay()
    }
    
    @IBOutlet weak var bookingImageView: UIImageView!
    @IBOutlet weak var bookingServicesLabel: UILabel!
    @IBOutlet weak var bookingDurationLabel: UILabel!
    @IBOutlet weak var bookingPriceLabel: UILabel!
    
    @IBOutlet weak var bookingDateLabel: UILabel!
    @IBOutlet weak var bookingTimeLabel: UILabel!
    
    func updateBookingInfoDisplay() {
        self.bookingServicesLabel.text = self.booking.descriptionOfServices
        self.bookingDurationLabel.text = String(self.booking.durationInMinutes())
        self.bookingPriceLabel.text = self.booking.priceStringInBaht()
        
        if self.booking.hasDate() {
            self.bookingDateLabel.text = self.dateFormatter.stringFromDate(self.booking.date!)
            self.bookingTimeLabel.text = self.timeFormatter.stringFromDate(self.booking.date!) + "-" + self.timeFormatter.stringFromDate(self.booking.endDate!)
            self.bookingDateLabel.hidden = false
            self.bookingTimeLabel.hidden = false
        } else {
            self.bookingDateLabel.hidden = true
            self.bookingTimeLabel.hidden = true
        }
    }

}
