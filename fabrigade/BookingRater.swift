//
//  BookingRater.swift
//  FabBrigade
//
//  Created by Kareem Sabri on 2015-10-11.
//  Copyright © 2015 FabBrigade. All rights reserved.
//

import Foundation

protocol BookingRaterDelegate {
    func bookingRater(bookingRater: BookingRater, didRateBooking booking: Booking)
    func bookingRater(bookingRater: BookingRater, failedToRateBooking booking: Booking)
}

protocol BookingRater: class {
    var bookingRaterDelegate: BookingRaterDelegate! { get }
    var bookingToRate: Booking! { get }
    var stars: Int { get }
    var comments: String { get }
    
    func rate()
}

extension BookingRater {
    func rate() {
        let rating = Rating()
        rating.stars = self.stars
        rating.comments = self.comments
        
        rating.saveInBackgroundWithBlock { (success, error) -> Void in
            if error != nil {
                self.bookingRaterDelegate.bookingRater(self, failedToRateBooking: self.bookingToRate)
            } else {
                self.bookingToRate.rating = rating
                self.bookingToRate.saveInBackground()
                self.bookingRaterDelegate.bookingRater(self, didRateBooking: self.bookingToRate)
            }
        }
    }
}